package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class ResearcherDAOImplementation implements ResearcherDAO {
	
	private static ResearcherDAOImplementation instance = null;
	private ResearcherDAOImplementation() {}
	public static ResearcherDAOImplementation getInstance() {
		if(null == instance) {
			instance = new ResearcherDAOImplementation();
		}
		return instance;
	}

	@Override
	public Researcher create(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try{
			session.beginTransaction();
			session.save(researcher);
			session.getTransaction().commit();
			} catch (Exception e) {
			}finally {
				session.close();
			}		
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		Researcher r = null;
		Session session = SessionFactoryService.get().openSession();
		try{
			session.beginTransaction();
			r = session.get(Researcher.class, researcherId);
			session.getTransaction().commit();
			} catch (Exception e) {
			}finally {
				session.close();
			}		
		return r;
	}

	@Override
	public Researcher update(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try{
			session.beginTransaction();
			session.saveOrUpdate(researcher);
			session.getTransaction().commit();
			} catch (Exception e) {
			}finally {
				session.close();
			}		
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try{
			session.beginTransaction();
			session.delete(researcher);
			session.getTransaction().commit();
			} catch (Exception e) {
			}finally {
				session.close();
			}		
		return researcher;
	}

	@Override
	public List<Researcher> readAll() {
		List<Researcher> r = null;
		Session session = SessionFactoryService.get().openSession();
		try{
			session.beginTransaction();
			r = (List<Researcher>) (session.createQuery("from Researcher").list());
			session.getTransaction().commit();
			} catch (Exception e) {
			}finally {
				session.close();
			}		
		return r;
	}

	@Override
	public Researcher readByEmail(String email) {
		for (Researcher r: this.readAll())
				if (email.contentEquals(r.getEmail()))
					return r;
		return null;
	}
	
	@Override //Returns all the researchers associated to a publication
	public List<Researcher> publications(String publicationId) {
		//Create empty list to return
		List<Researcher> researchersList = new ArrayList<Researcher>();
		
		//Get open session - created in given class
		Session session = SessionFactoryService.get().openSession();
		Publication publication = null; 
		
		//Read publication using its id
		try{
			//Operation against DB
			session.beginTransaction();
			//Read publication - persistence operation
			publication = session.get(Publication.class, publicationId);
			//In case everything goes well - We commit
			session.getTransaction().commit();
		}catch(Exception e){
			//Handle exceptions
		}finally{
			//Always close session at the end of its use - consistent state
			session.close();
		}
		
		//Get publication authors as a list
		String authors = publication.getAuthors();
		List<String> authorIds = (Arrays.asList(authors.split(";")));
		
		//Create list of researchers
		for (String authorId : authorIds) {
			//Read researcher from DB
			//Get open session - created in given class
			session = SessionFactoryService.get().openSession();
			Researcher researcher = null;
			
			//In case the operations sent to the DB fail -> close session
			try{
				//Operation against DB
				session.beginTransaction();
				//Read operation - persistence operation
				researcher = session.get(Researcher.class, authorId);
				//In case everything goes well - We commit
				session.getTransaction().commit();
			}catch(Exception e){
				//Handle exceptions
			}finally{
				//Always close session at the end of its use - consistent state
				session.close();
			}
			//Add researcher to the list
			if (researcher != null)
				researchersList.add(researcher);
		}
		
		return researchersList;
	}


}
