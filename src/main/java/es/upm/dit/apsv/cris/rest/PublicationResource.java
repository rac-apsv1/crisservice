package es.upm.dit.apsv.cris.rest;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;


import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;


@Path("/Publications") //A todos ellos se llegan en la ruta /Publications
public class PublicationResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Publication> readAll() { //Cogera todo en formato JSON
		return PublicationDAOImplementation.getInstance().readAll(); //para crear el objeto es con el getInstance eh, nada de new blablabla
	}
	

	@POST //Para crear un Publication y se llama al create
	@Consumes(MediaType.APPLICATION_JSON) //Dice que esto viene en el cuerpo de la petici�n en formato JSON. Se pone cuando se coge algo del cuerpo
	public Response create(Publication rnew) throws URISyntaxException {
	    Publication p = PublicationDAOImplementation.getInstance().create(rnew);	
	    URI uri = new URI("/CRISSERVICE/rest/Publications/" + p.getId());	
	    return Response.created(uri).build();
	}
	
	@GET
	@Path("{id}/Researchers") //this notation means it has to be appended to the path
	@Produces(MediaType.APPLICATION_JSON)
	public List<Researcher> readResearchersId(@PathParam("id") String id) {
        return ResearcherDAOImplementation.getInstance().publications(id);
    }

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response read(@PathParam("id") String id) {
		Publication p = PublicationDAOImplementation.getInstance().read(id);
		if (p == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.ok(p, MediaType.APPLICATION_JSON).build(); //Responde ok y con un objeto (el Publication) si todo va bien
	}        

	@POST
	@Consumes(MediaType.APPLICATION_JSON) //Aqui como se coge algo del cuerpo pues se pone Consumes
	@Path("{id}")
	public Response update(@PathParam("id") String id, Publication r) {
		Publication pld = PublicationDAOImplementation.getInstance().read(id);
		if ((pld == null) || (! pld.getId().contentEquals(r.getId())))
			return Response.notModified().build();
		PublicationDAOImplementation.getInstance().update(r);
		return Response.ok().build();                
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") String  id) {
		Publication pld = PublicationDAOImplementation.getInstance().read(id);
		if (pld == null)
			return Response.notModified().build();
		PublicationDAOImplementation.getInstance().delete(pld);
		return Response.ok().build();

	}
	
	@GET
	@Path("{id}")
	public Response updateCiteNumber(@PathParam("id") String id) {
		String APIKey = "99aad75a6ba0e7643772680f4945bed0";
		Publication p = PublicationDAOImplementation.getInstance().read(id);
		if (p == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		Client client = ClientBuilder.newClient(new ClientConfig());
		JsonObject o = client.register(JsonProcessingFeature.class).target(
				"https://api.elsevier.com/content/search/scopus?query=SCOPUS-ID("+ id + ")&field=citedby-count")
				.request().accept(MediaType.APPLICATION_JSON)
				.header("X-ELS-APIKey", APIKey)
				.get(JsonObject.class);	
		JsonArray a =((JsonArray) ((JsonObject) o.get("search-results")).get("entry"));
		JsonObject oo = a.getJsonObject(0);
		int ncites = Integer.parseInt(oo.get("citedby-count").toString().replaceAll("\"", ""));
		p.setCiteCount(ncites);
		PublicationDAOImplementation.getInstance().update(p);
		return Response.ok().build();
	}

	
}
